#lang racket/base

;
; Опишите функцию (minilist-cps lst), возвращающую минимум непустого списка чисел,
; используя стиль передачи отсторченных вычислений.
;
(define (minilist-cps lst cc)
  (if
    (null? (cdr lst))
    (cc (car lst))
    (minilist-cps (cdr lst) (lambda (x) (min (car lst) (cc x))))))

(define (minilist-loop lst res)
  (if
    (null? (cdr lst))
    (min (car lst) res)
    (minilist-loop (cdr lst) (min (car lst) res))))

(define (length-cps lst cc)
  (if
    (null? lst)
    (cc 0)
    (length-cps (cdr lst) (lambda (x) (+ 1 (cc x))))))

(define (minilist-ars lst cc)
  (if
    (null? (cdr lst))
    (cc (car lst))
    (minilist-ars (cdr lst) (lambda (x) (min (cc x) (car lst))))))

;(define (rand-list n bot top cc)
  ;(define (rand bot top)
    ;(+ bot (random (- top bot))))
  ;(if (= n 1)
    ;(cc (list (rand bot top)))
    ;(rand-list (- n 1) top (lambda (x) (cons (random top) (cc x))))))

(define (fact-iter n res)
  (if (= n 1)
    res
    (fact-iter (- n 1) (* res n))))

(define (fact-cps n cc)
  (if (= n 1)
    (cc 1)
    (fact-cps (- n 1) (lambda (x) (cc (* x n))))))

(fact-iter 3 1)

(define (make-rat x y)
  (/ x y))

(make-rat 2 4)

;(let ((lst (rand-list 10 10 (lambda (x) x))))
      ;(print lst)
      ;(newline)
      ;(print (minilist-loop lst (car lst)))
      ;(newline)
      ;(print (minilist-cps lst (lambda (x) x))))
